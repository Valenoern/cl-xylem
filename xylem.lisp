;;; xylem.lisp - xylem library for bopwiki etc
(defpackage :xylem
	;; it is not recommended to :use this library into other packages
	;; currently everything that uses it will use it by nickname
	(:use :common-lisp)
	;;(:local-nicknames
	;;)
	(:export
		;; #::*great-table*
		
		#:xylem-init
		#:xylem-value  ; '(setf xylem-value)
			;; XYLEM-VALUE is reused as an arbitrary symbol to store further properties at a given node
		#:xylem-value-or-node
))
(in-package :xylem)

(defparameter *great-table* nil)  ; hash table that by default everything goes in


(defun xylem-init () ; create table(s) {{{
	(setq *great-table* (make-hash-table :test 'equal))
) ; }}}

(defun xylem-value (property  &key table set-value) ; get/store value in table {{{
	(when (null table)  (setq table *great-table*))
	
	(cond
		;; if not told to set value, get value
		((null set-value)  (gethash property table))
		(t  ; otherwise set value
			(let (node value)
			(setf (gethash property table) set-value)
			
			;; if key is a list, keep track of related keys
			(when (and (listp property) (> (length property) 1))
				(setf
					node
						(cons 'xylem-value (butlast property))
					(gethash node table)
						(pushnew (nth 0 (last property)) (gethash node table))
			))
	)))
)

(defun (setf xylem-value) (value property  &key table)
	;; I am mystified how these setf definitions work. why do you have to write '(value property) if the function is called '(property value)
	(xylem-value property :set-value value :table table))
; ) }}}

(defun xylem-value-or-node (node item-id  &key all) ; get individual thing or all such things {{{
	(unless (listp node)
		(setq node (list node)))
	
	(if (null all)
		;; in most cases, get a single value
		(xylem-value (append node (list item-id)))
		;; if passed :all t, get all things registered at this key
		(xylem-value (cons 'xylem-value node))
		)
) ; }}}

