(defsystem "xylem"
	:author "Valenoern"
	:licence "AGPL"
		;; as a somewhat general-purpose library that could maybe even find its way into rdf-based platforms, this one definitely needs to be AGPL
	:description "tuple store library for bopwiki, shardquest, etc"
		;; it's xylem to fit with the general bop theme of plant shoots/branches
	
	;;:depends-on (
		;;"uiop"
	;;)
	:components (
		(:file "xylem")  ; :xylem
))
